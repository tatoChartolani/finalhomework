package com.example.myapp
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()

        init()
    }



    private fun init() {
        readData()
        auth = FirebaseAuth.getInstance()
        signinButton.setOnClickListener {
            Empty()
            saveData()
            val intent = Intent(this, Profile::class.java)
            intent.putExtra("email",emailField.text.toString())
            intent.putExtra("password",passwordField.text.toString())
            startActivity(intent)
        }

        signupButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }


    }


    private fun Empty() {
        signinButton.setOnClickListener {
            if (emailField.text.isNotEmpty() && passwordField.text.isNotEmpty()) {
                LogIn()
            } else {
                Toast.makeText(applicationContext, "LOG IN FAILED", Toast.LENGTH_SHORT).show()
            }

        }

    }

    private fun LogIn(){
        auth.signInWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("log in", "signInWithEmail:success")
                    val intent = Intent(this, Profile::class.java)
                    startActivity(intent)
                } else {

                    Log.w("log in", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun saveData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email", emailField.text.toString())
        edit.putString("password",passwordField.text.toString())
        edit.commit()

    }

    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailField.setText(sharedPreferences.getString("email",""))
        passwordField.setText(sharedPreferences.getString("password",""))

    }
}

